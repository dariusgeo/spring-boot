package sda.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import sda.spring.service.EmployeeService;

@SpringBootApplication
public class Application { //implements CommandLineRunner{
	
//	@Autowired
//	private EmployeeService employeeService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
//    public void run(String... args) throws Exception {
//        Iterable<sda.spring.model.Employee> employeeList = employeeService.findAll();
//        for(sda.spring.model.Employee employee:employeeList){
//            System.out.println("Employee : " + employee.getEmpNo() + " " + employee.getFirstName() + " " + employee.getLastName());
//        }
//    }
}
