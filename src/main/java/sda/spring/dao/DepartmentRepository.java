package sda.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sda.spring.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
	 
}
