package sda.spring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sda.spring.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	List<Employee> findAllByLastName(String lastname);
	
	List<Employee> findByDepartments_DeptName(String deptName);
}
