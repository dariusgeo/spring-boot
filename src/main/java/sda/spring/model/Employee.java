package sda.spring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "employees")
public class Employee implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_no")
	private int empNo;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "birth_date")
	@Temporal(TemporalType.DATE)
	/*
	 TIME (java.sql.Time)
	 DATE(java.sql.Date) 
	 TIMESTAMP (java.sql.Timestamp) 
	 */
	private Date birthDate;
	
	@Column(name = "hire_date")
	@Temporal(TemporalType.DATE)
	private Date hireDate;
	
	@Column(name = "gender")
	@Enumerated(EnumType.STRING)
	private EGender gender;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "dept_emp", 
        joinColumns = { @JoinColumn(name = "emp_no") }, 
        inverseJoinColumns = { @JoinColumn(name = "dept_no") }
    )
    Set<Department> departments = new HashSet<>();
	
//	@OneToMany(mappedBy="employee")
//    private Set<Salary> salaries;
//	
//	@OneToMany(mappedBy="employee")
//    private Set<Title> titles;

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public EGender getGender() {
		return gender;
	}

	public void setGender(EGender gender) {
		this.gender = gender;
	}

	public Set<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

//	public Set<Salary> getSalaries() {
//		return salaries;
//	}
//
//	public void setSalaries(Set<Salary> salaries) {
//		this.salaries = salaries;
//	}
//
//	public Set<Title> getTitles() {
//		return titles;
//	}
//
//	public void setTitles(Set<Title> titles) {
//		this.titles = titles;
//	}	
}
