//package sda.spring.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//@Entity
//@Table(name = "titles")
//public class Title {
//
////	@Id
////	//@GeneratedValue(strategy = GenerationType.IDENTITY)
////	@Column(name = "emp_no")
////	private int empNo;
//	
//	@Column(name = "from_date")
//	@Temporal(TemporalType.DATE)
//	private Date fromDate;
//	
//	@Column(name = "to_date")
//	@Temporal(TemporalType.DATE)
//	private Date toDate;
//	
//	@Column(name = "title")
//	private String title;
//	
//	@ManyToOne
//	@JoinColumn(name="emp_no", nullable=false)
//	private Employee employee;
//
//	public Date getFromDate() {
//		return fromDate;
//	}
//
//	public void setFromDate(Date fromDate) {
//		this.fromDate = fromDate;
//	}
//
//	public Date getToDate() {
//		return toDate;
//	}
//
//	public void setToDate(Date toDate) {
//		this.toDate = toDate;
//	}
//
//	public String getTitle() {
//		return title;
//	}
//
//	public void setTitle(String title) {
//		this.title = title;
//	}
//
//	public Employee getEmployee() {
//		return employee;
//	}
//
//	public void setEmployee(Employee employee) {
//		this.employee = employee;
//	}
//
//}
