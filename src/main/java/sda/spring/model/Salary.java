//package sda.spring.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//
//@Entity
//@Table(name = "salaries")
//public class Salary {
//	
//	//@Id
//	//@GeneratedValue(strategy = GenerationType.IDENTITY)
//	//@Column(name = "emp_no")
//	//private int empNo;
//	
//	@Column(name = "from_date")
//	@Temporal(TemporalType.DATE)
//	private Date fromDate;
//	
//	@Column(name = "to_date")
//	@Temporal(TemporalType.DATE)
//	private Date toDate;
//	
//	@Column(name = "salary")
//	private Long salary;
//	
//	@ManyToOne
//    @JoinColumn(name="emp_no", nullable=false)
//	private Employee employee;
//
//
//	public Date getFromDate() {
//		return fromDate;
//	}
//
//	public void setFromDate(Date fromDate) {
//		this.fromDate = fromDate;
//	}
//
//	public Date getToDate() {
//		return toDate;
//	}
//
//	public void setToDate(Date toDate) {
//		this.toDate = toDate;
//	}
//
//	
//	public Long getSalary() {
//		return salary;
//	}
//
//	public void setSalary(Long salary) {
//		this.salary = salary;
//	}
//
//	public Employee getEmployee() {
//		return employee;
//	}
//
//	public void setEmployee(Employee employee) {
//		this.employee = employee;
//	}
//	
//}
