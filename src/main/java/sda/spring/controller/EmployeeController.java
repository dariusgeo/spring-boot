package sda.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sda.spring.dao.EmployeeRepository;

@Controller
@RequestMapping(path = "/api")
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("employees", employeeRepository.findAll());
		return "employeeList";
	}
}
